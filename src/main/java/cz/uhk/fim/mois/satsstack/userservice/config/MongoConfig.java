package cz.uhk.fim.mois.satsstack.userservice.config;

import com.mongodb.Block;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.SslSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;

/**
 * @author : vanya.melnykovych
 * @since : 15.03.21
 */
@Configuration
@EnableMongoRepositories(basePackages = "cz.uhk.fim.mois.satsstack.userservice.repository")
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Value("${mongo.db.uri}")
    private String uri;

    @Value("${mongodb.database}")
    private String database;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString(uri);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .applyToSslSettings(getSSLSetting())
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton("cz.uhk.fim.mois.satsstack.userservice");
    }

    private SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        final SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, null, new SecureRandom());
        return sslContext;
    }

    private Block<SslSettings.Builder> getSSLSetting() {
        return builder -> {
            try {
                builder.enabled(true).context(getSSLContext());
            }  catch (NoSuchAlgorithmException | KeyManagementException e) {
                builder.enabled(false);
            }
        };
    }
}
