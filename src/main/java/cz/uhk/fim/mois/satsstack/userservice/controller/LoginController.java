package cz.uhk.fim.mois.satsstack.userservice.controller;

import cz.uhk.fim.mois.satsstack.userservice.domain.User;
import cz.uhk.fim.mois.satsstack.userservice.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.security.Principal;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@RestController
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public ResponseEntity<Void> loginUser(Principal principal) {
        User user = userService.extractUserFromAuthInfo(principal);
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create("https://satsstack.app/login/callback?token=" + user.getToken())).build();
    }

    @GetMapping("/login")
    public ResponseEntity<Void> method() {
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create("https://satsstack.app/login")).build();
    }
}
