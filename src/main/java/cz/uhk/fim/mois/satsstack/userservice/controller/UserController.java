package cz.uhk.fim.mois.satsstack.userservice.controller;

import cz.uhk.fim.mois.satsstack.userservice.domain.User;
import cz.uhk.fim.mois.satsstack.userservice.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 15.03.21
 */
@RestController
@RequestMapping("rest/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/find")
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/get/token")
    public User getUserByToken(@RequestHeader("Authorization") String token) {
        return userService.findByToken(token);
    }

    @GetMapping("/verify")
    public Boolean verifyUser(@RequestHeader("Authorization") String token) {
        return userService.findByToken(token) != null;
    }

}
