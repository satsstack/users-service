package cz.uhk.fim.mois.satsstack.userservice.domain;

/**
 * @author : vanya.melnykovych
 * @since : 21.03.21
 */
public enum SocialProvider {
    GOOGLE,
    FACEBOOK
}
