package cz.uhk.fim.mois.satsstack.userservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * @author : vanya.melnykovych
 * @since : 15.03.21
 */
@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String id;

    private String name;

    private String userPic;

    private String email;

    private String locale;

    private LocalDateTime lastVisit;

    private SocialProvider provider;

    private String token;
}
