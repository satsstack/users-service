package cz.uhk.fim.mois.satsstack.userservice.filter;

import cz.uhk.fim.mois.satsstack.userservice.domain.User;
import cz.uhk.fim.mois.satsstack.userservice.service.UserService;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * @author : vanya.melnykovych
 * @since : 22.03.21
 */
@Component
public class TokenRequestFilter extends OncePerRequestFilter {

    private final UserService userService;

    public TokenRequestFilter(UserService userService) {
        this.userService = userService;
    }
    
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path = request.getRequestURI();
        Set<String> permited = Set.of("/health", "/",
                                      "/health/liveness", 
                                      "/health/readiness", 
                                      "/mappings",
                                      "/info", 
                                      "/v3/api-docs",
                                      "/v3/api-docs/**",
                                      "/v3/api-docs/swagger-config",
                                      "/swagger-ui/oauth2-redirect.html",
                                      "/swagger-ui.html",
                                      "/swagger-ui/**");
        return permited.contains(path);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = httpServletRequest.getHeader("Authorization");
        String name = httpServletRequest.getQueryString();
        User user;

        if (requestTokenHeader != null && !requestTokenHeader.isEmpty()) {
            user = userService.findByToken(requestTokenHeader);
            if (user != null) {
                if (SecurityContextHolder.getContext().getAuthentication() == null) {
                    final RememberMeAuthenticationToken rememberMeAuthenticationToken = new RememberMeAuthenticationToken(user.getId(), user.getToken(), null);
                    rememberMeAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(rememberMeAuthenticationToken);
                }
                if (name != null &&
                        name.equals("logout")) {
                    SecurityContextHolder.clearContext();
                    userService.logoutUser(user);
                }
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
