package cz.uhk.fim.mois.satsstack.userservice.repository;

import cz.uhk.fim.mois.satsstack.userservice.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author : vanya.melnykovych
 * @since : 15.03.21
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findByToken(String token);
}
