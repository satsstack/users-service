package cz.uhk.fim.mois.satsstack.userservice.service;

import cz.uhk.fim.mois.satsstack.userservice.domain.SocialProvider;
import cz.uhk.fim.mois.satsstack.userservice.domain.User;
import cz.uhk.fim.mois.satsstack.userservice.repository.UserRepository;
import cz.uhk.fim.mois.satsstack.userservice.utils.DetailsUtil;
import lombok.NonNull;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author : vanya.melnykovych
 * @since : 16.03.21
 */
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private User createUserByGoogle(Map<String, String> details) {
        final User newUser = new User();

        newUser.setName(details.get(DetailsUtil.AUTH_DETAILS_NAME_PARAM));
        newUser.setEmail(details.get(DetailsUtil.AUTH_DETAILS_EMAIL_PARAM));
        newUser.setLocale(details.get(DetailsUtil.GOOGLE_LOCALE_FILED_NAME));
        newUser.setUserPic(details.get(DetailsUtil.GOOGLE_PICTURE_FIELD_NAME));
        newUser.setProvider(SocialProvider.GOOGLE);

        return newUser;
    }

    private User createUserByFacebook(Map<String, String> details) {
        final User newUser = new User();

        newUser.setName(details.get(DetailsUtil.AUTH_DETAILS_NAME_PARAM));
        newUser.setEmail(details.get(DetailsUtil.AUTH_DETAILS_EMAIL_PARAM));
        newUser.setProvider(SocialProvider.FACEBOOK);

        return newUser;
    }

    private User createUser(OAuth2AuthenticationDetails oAuth2AuthenticationDetails, Map<String, String> details) {
        final String id = getIdForUser(details);
        final User user = userRepository.findById(id).orElseGet(() -> {
           return isGoogle(details) ? createUserByGoogle(details)
                   : createUserByFacebook(details);
        });
        user.setId(id);
        user.setLastVisit(LocalDateTime.now());
        user.setToken(oAuth2AuthenticationDetails.getTokenValue());
        userRepository.save(user);
        return user;
    }

    private String getIdForUser(Map<String, String> details) {
        if(isGoogle(details)) {
            return details.get(DetailsUtil.GOOGLE_ID_FIELD_NAME);
        }
        return details.get(DetailsUtil.FACEBOOK_ID_FIELD_NAME);
    }

    public User extractUserFromAuthInfo(@NonNull Principal principal) {
        if (principal instanceof OAuth2Authentication) {
            return extractExternalUser(principal);
        }
        return new User();
    }

    private User extractExternalUser(@NonNull Principal principal) {
        OAuth2Authentication oAuth = (OAuth2Authentication) principal;
        Map<String, String> details = (Map<String, String>) oAuth.getUserAuthentication().getDetails();
        OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails) oAuth.getDetails();

        return createUser(oAuth2AuthenticationDetails, details);
    }

    private boolean isGoogle(@NonNull Map<String, String> details) {
        return details.containsKey(DetailsUtil.GOOGLE_ID_FIELD_NAME);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByToken(String token) {
        return userRepository.findByToken(token);
    }

    public User logoutUser(User user) {
        user.setToken(null);
        userRepository.save(user);
        return user;
    }
}
