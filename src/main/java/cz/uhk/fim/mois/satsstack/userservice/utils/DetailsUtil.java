package cz.uhk.fim.mois.satsstack.userservice.utils;

/**
 * @author : vanya.melnykovych
 * @since : 22.03.21
 */
public final class DetailsUtil {

    public static final String FACEBOOK_ID_FIELD_NAME = "id";

    public static final String GOOGLE_ID_FIELD_NAME = "sub";
    public static final String GOOGLE_LOCALE_FILED_NAME = "locale";
    public static final String GOOGLE_PICTURE_FIELD_NAME = "picture";

    public static final String AUTH_DETAILS_NAME_PARAM = "name";
    public static final String AUTH_DETAILS_EMAIL_PARAM = "email";
}
